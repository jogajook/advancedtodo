import ApiService from "./framework/api-service";
import {TodoList} from "./models/todo-list.model";
import {TodoListView} from "./components/todo-list/toto-list.component";
import {SpeechBoxView} from "./components/spech-box/speech-box.component";
import {ControlView} from "./components/controls/constrols.component";
import {CommandInvoker} from "./framework/command-invoker";
import {ITokenizer, Tokenizer, TokenizeStrategyParams} from "./models/tokenizer/tokenizer.model";
import {AppConfig} from "./models/config.model";
import {PointerTokenizeStrategy} from "./models/tokenizer/tokenize-strategy-pointer.model";
import {SpeechRecognitionWrapper} from "./framework/speech-recognition";
import {TodoStrategyOptions} from "./models/todo-strategy-options.model";
import {ITodo, Todo} from "./models/todo.model";
import {CapitalizeFirstLetterTokenizerDecorator} from "./models/tokenizer/tokenizer-decorator-capitalize-first-letter.model";
import {TrimTokenizerDecorator} from "./models/tokenizer/tokenizer-decorator-trim.model";
import {AddTodoCommand, RemoveTodoCommand} from "./models/commands.model";
import {SpaceTokenizeStrategy} from "./models/tokenizer/tokenize-strategy-space.model";

class App {
    todoList: TodoList;
    private todoListView: TodoListView;
    private speechBoxView: SpeechBoxView;
    private controlView: ControlView;
    private commandInvoker: CommandInvoker = new CommandInvoker();
    private tokenizer: ITokenizer;
    private recognition: any;
    private apiService = new ApiService(AppConfig.apiBaseUrl);

    constructor() {
        this.setTokenizer(
            new CapitalizeFirstLetterTokenizerDecorator(
                new TrimTokenizerDecorator(
                    new Tokenizer(new PointerTokenizeStrategy())))
        );

        this.loadData().then(() => {
            this.updateView();
            this.initRecognition();
        });
    }

    initRecognition() {
        this.recognition = SpeechRecognitionWrapper.createSpeechRecognition($("#start-speech"), {
            onResultReady: (text) => {
                this.speechBoxView.setText(text);
            }
        });
    }

    checkIsShowCreateTodosBtn(selectedStrategy: string) {
        this.speechBoxView.setIsShowCreateTodos(Number(selectedStrategy) !== TodoStrategyOptions.Pointer);
    }

    loadData() {
        return this.apiService.getList().then((data: ITodo[]) => {
            this.updateTodoList(data);
            return data;
        });
    }

    updateTodoList(data: ITodo[]): TodoList {
        this.todoList = new TodoList();
        data.forEach(item => this.todoList.add(new Todo(item.name, item.id, item.date, item.status)));
        return this.todoList;
    }

    updateView() {
        this.updateTodoListView();
        this.updateSpeechBoxView();
        this.updateControlView();
    }

    private updateTodoListView() {
        this.todoListView = new TodoListView(this.todoList, {
            onCompleteClickCb: (todo: Todo, $todoEl) => {
                todo.setDone();
                this.apiService.update(todo);
            },
            onRemoveClickCb: (todo: Todo, $todoEl) => {
                this.removeTodo(todo).then(() => $todoEl.slideUp());
            },
            onRestoreClickCb: (todo: Todo, $todoEl) => {
                todo.setUndone();
                this.apiService.update(todo);
            },
            onSaveClickCb: (todo: Todo, $todoEl, todoText: string) => {
                todo.setName(todoText);
                this.apiService.update(todo);
            },
            callBackContext: this
        });
        this.todoListView.render();
    }

    private updateSpeechBoxView() {
        this.speechBoxView = new SpeechBoxView({
            onTextClickCb: (todoSource: string, charPos: number) => {
                const tokens = this.getTokens(todoSource, {endCharPos: charPos});
                if (tokens.length && tokens[0]) {
                    this.addTodo(tokens[0]);
                    this.speechBoxView.setText(todoSource.slice(charPos, todoSource.length));
                }
            },
            onCreateTodosClickCb: (todoSource: string) => {
                const tokens = this.getTokens(todoSource);
                if (tokens.length) {
                    this.addTodos(tokens);
                    this.speechBoxView.setText("");
                }
            },
            onStrategyChangeCb: (todoSource: string, strategy: string) => {
                this.tokenizer.setTokenizeStrategy
                (Number(strategy) === TodoStrategyOptions.Pointer
                    ? new PointerTokenizeStrategy()
                    : new SpaceTokenizeStrategy());
                this.checkIsShowCreateTodosBtn(strategy);
            },
        });
        this.speechBoxView.render();
    }


    private updateControlView() {
        this.controlView = new ControlView(this.commandInvoker);
        this.controlView.render();
    }

    setTokenizer(tokenizer: ITokenizer) {
        this.tokenizer = tokenizer;
    }

    getTokens(todoSource: string, params?: TokenizeStrategyParams): string[] {
        return this.tokenizer.tokenize(todoSource, params);
    }

    addTodos(todoNames: string[]) {
        todoNames.forEach(this.addTodo.bind(this));
    }

    addTodo(tokenOrTodo: string | Todo) {
        const todo = typeof tokenOrTodo === "string" ? new Todo(tokenOrTodo) : tokenOrTodo;
        let command = new AddTodoCommand(this.todoList, todo, this.apiService);
        this.commandInvoker.storeAndExecute(command);
    }

    removeTodo(todo: Todo): Promise<any> {
        const command = new RemoveTodoCommand(this.todoList, todo, this.apiService);
        return this.commandInvoker.storeAndExecute(command)
    }
}

new App();