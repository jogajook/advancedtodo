define(["require", "exports", "./framework/api-service", "./models/todo-list.model", "./components/todo-list/toto-list.component", "./components/spech-box/speech-box.component", "./components/controls/constrols.component", "./framework/command-invoker", "./models/tokenizer/tokenizer.model", "./models/config.model", "./models/tokenizer/tokenize-strategy-pointer.model", "./framework/speech-recognition", "./models/todo-strategy-options.model", "./models/todo.model", "./models/tokenizer/tokenizer-decorator-capitalize-first-letter.model", "./models/tokenizer/tokenizer-decorator-trim.model", "./models/commands.model", "./models/tokenizer/tokenize-strategy-space.model"], function (require, exports, api_service_1, todo_list_model_1, toto_list_component_1, speech_box_component_1, constrols_component_1, command_invoker_1, tokenizer_model_1, config_model_1, tokenize_strategy_pointer_model_1, speech_recognition_1, todo_strategy_options_model_1, todo_model_1, tokenizer_decorator_capitalize_first_letter_model_1, tokenizer_decorator_trim_model_1, commands_model_1, tokenize_strategy_space_model_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var App = /** @class */ (function () {
        function App() {
            var _this = this;
            this.commandInvoker = new command_invoker_1.CommandInvoker();
            this.apiService = new api_service_1.default(config_model_1.AppConfig.apiBaseUrl);
            this.setTokenizer(new tokenizer_decorator_capitalize_first_letter_model_1.CapitalizeFirstLetterTokenizerDecorator(new tokenizer_decorator_trim_model_1.TrimTokenizerDecorator(new tokenizer_model_1.Tokenizer(new tokenize_strategy_pointer_model_1.PointerTokenizeStrategy()))));
            this.loadData().then(function () {
                _this.updateView();
                _this.initRecognition();
            });
        }
        App.prototype.initRecognition = function () {
            var _this = this;
            this.recognition = speech_recognition_1.SpeechRecognitionWrapper.createSpeechRecognition($("#start-speech"), {
                onResultReady: function (text) {
                    _this.speechBoxView.setText(text);
                }
            });
        };
        App.prototype.checkIsShowCreateTodosBtn = function (selectedStrategy) {
            this.speechBoxView.setIsShowCreateTodos(Number(selectedStrategy) !== todo_strategy_options_model_1.TodoStrategyOptions.Pointer);
        };
        App.prototype.loadData = function () {
            var _this = this;
            return this.apiService.getList().then(function (data) {
                _this.updateTodoList(data);
                return data;
            });
        };
        App.prototype.updateTodoList = function (data) {
            var _this = this;
            this.todoList = new todo_list_model_1.TodoList();
            data.forEach(function (item) { return _this.todoList.add(new todo_model_1.Todo(item.name, item.id, item.date, item.status)); });
            return this.todoList;
        };
        App.prototype.updateView = function () {
            this.updateTodoListView();
            this.updateSpeechBoxView();
            this.updateControlView();
        };
        App.prototype.updateTodoListView = function () {
            var _this = this;
            this.todoListView = new toto_list_component_1.TodoListView(this.todoList, {
                onCompleteClickCb: function (todo, $todoEl) {
                    todo.setDone();
                    _this.apiService.update(todo);
                },
                onRemoveClickCb: function (todo, $todoEl) {
                    _this.removeTodo(todo).then(function () { return $todoEl.slideUp(); });
                },
                onRestoreClickCb: function (todo, $todoEl) {
                    todo.setUndone();
                    _this.apiService.update(todo);
                },
                onSaveClickCb: function (todo, $todoEl, todoText) {
                    todo.setName(todoText);
                    _this.apiService.update(todo);
                },
                callBackContext: this
            });
            this.todoListView.render();
        };
        App.prototype.updateSpeechBoxView = function () {
            var _this = this;
            this.speechBoxView = new speech_box_component_1.SpeechBoxView({
                onTextClickCb: function (todoSource, charPos) {
                    var tokens = _this.getTokens(todoSource, { endCharPos: charPos });
                    if (tokens.length && tokens[0]) {
                        _this.addTodo(tokens[0]);
                        _this.speechBoxView.setText(todoSource.slice(charPos, todoSource.length));
                    }
                },
                onCreateTodosClickCb: function (todoSource) {
                    var tokens = _this.getTokens(todoSource);
                    if (tokens.length) {
                        _this.addTodos(tokens);
                        _this.speechBoxView.setText("");
                    }
                },
                onStrategyChangeCb: function (todoSource, strategy) {
                    _this.tokenizer.setTokenizeStrategy(Number(strategy) === todo_strategy_options_model_1.TodoStrategyOptions.Pointer
                        ? new tokenize_strategy_pointer_model_1.PointerTokenizeStrategy()
                        : new tokenize_strategy_space_model_1.SpaceTokenizeStrategy());
                    _this.checkIsShowCreateTodosBtn(strategy);
                },
            });
            this.speechBoxView.render();
        };
        App.prototype.updateControlView = function () {
            this.controlView = new constrols_component_1.ControlView(this.commandInvoker);
            this.controlView.render();
        };
        App.prototype.setTokenizer = function (tokenizer) {
            this.tokenizer = tokenizer;
        };
        App.prototype.getTokens = function (todoSource, params) {
            return this.tokenizer.tokenize(todoSource, params);
        };
        App.prototype.addTodos = function (todoNames) {
            todoNames.forEach(this.addTodo.bind(this));
        };
        App.prototype.addTodo = function (tokenOrTodo) {
            var todo = typeof tokenOrTodo === "string" ? new todo_model_1.Todo(tokenOrTodo) : tokenOrTodo;
            var command = new commands_model_1.AddTodoCommand(this.todoList, todo, this.apiService);
            this.commandInvoker.storeAndExecute(command);
        };
        App.prototype.removeTodo = function (todo) {
            var command = new commands_model_1.RemoveTodoCommand(this.todoList, todo, this.apiService);
            return this.commandInvoker.storeAndExecute(command);
        };
        return App;
    }());
    new App();
});
//# sourceMappingURL=app.js.map