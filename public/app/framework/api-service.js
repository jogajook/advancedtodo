define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ApiService = /** @class */ (function () {
        function ApiService(baseUrl, settings) {
            if (settings === void 0) { settings = { dataType: "json", contentType: 'application/json; charset=utf-8' }; }
            this.baseUrl = baseUrl;
            this.settings = settings;
        }
        ApiService.prototype.getList = function () {
            return $.get(this.baseUrl);
        };
        ApiService.prototype.getOne = function (id) {
            return $.get(this.baseUrl + "/" + id);
        };
        ApiService.prototype.create = function (data) {
            return $.ajax({
                type: "POST",
                url: this.baseUrl,
                data: JSON.stringify(data),
                contentType: this.settings.contentType,
                dataType: this.settings.dataType
            });
        };
        ApiService.prototype.update = function (data) {
            return $.ajax({
                url: this.baseUrl + "/" + data.id,
                type: 'PUT',
                data: JSON.stringify(data),
                contentType: this.settings.contentType,
                dataType: this.settings.dataType
            });
        };
        ApiService.prototype.delete = function (id) {
            return $.ajax({
                url: this.baseUrl + "/" + id,
                type: 'DELETE'
            });
        };
        return ApiService;
    }());
    exports.default = ApiService;
});
//# sourceMappingURL=api-service.js.map