define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SpeechRecognitionWrapper = /** @class */ (function () {
        function SpeechRecognitionWrapper($speechBtnEl, opts) {
            this.$speechBtnEl = $speechBtnEl;
            this.recognition = new window["webkitSpeechRecognition"]();
            this.$speechBtnEl = $speechBtnEl;
            this.options = Object.assign(SpeechRecognitionWrapper.options, opts);
            this.isRunning = false;
            this.setup();
        }
        SpeechRecognitionWrapper.createSpeechRecognition = function ($speechBtnEl, opts) {
            if (window.hasOwnProperty('webkitSpeechRecognition')) {
                var recognition = new SpeechRecognitionWrapper($speechBtnEl, opts);
                return recognition;
            }
            return null;
        };
        SpeechRecognitionWrapper.prototype.setup = function () {
            var opts = this.options;
            this.recognition.onresult = function (event) {
                opts.onResultReady(event.results[0][0].transcript);
            };
            this.$speechBtnEl.click(this.toggle.bind(this));
        };
        SpeechRecognitionWrapper.prototype.toggle = function () {
            this.isRunning ? this.stop() : this.start();
        };
        SpeechRecognitionWrapper.prototype.start = function () {
            this.isRunning = true;
            this.$speechBtnEl
                .removeClass(this.options.stoppedClass)
                .addClass(this.options.runningClass)
                .text(this.options.runningBtnTitle);
            this.recognition.start();
        };
        SpeechRecognitionWrapper.prototype.stop = function () {
            this.isRunning = false;
            this.$speechBtnEl
                .removeClass(this.options.runningClass)
                .addClass(this.options.stoppedClass)
                .text(this.options.stoppedBtnTitle);
            this.recognition.stop();
        };
        SpeechRecognitionWrapper.options = {
            lang: "ru-RU",
            runningClass: "btn-warning",
            stoppedClass: "btn-danger",
            runningBtnTitle: "Stop Speaking",
            stoppedBtnTitle: "Start Speaking",
            onResultReady: function (text) {
            }
        };
        return SpeechRecognitionWrapper;
    }());
    exports.SpeechRecognitionWrapper = SpeechRecognitionWrapper;
});
//# sourceMappingURL=speech-recognition.js.map