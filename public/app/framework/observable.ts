import {IObserver} from "./observer";

export interface IObservable {
    addObserver(observer: IObserver);
    notifyObservers();
}

export abstract class Observable implements IObservable {

    private observers: IObserver[] = [];

    addObserver(observer: IObserver) {
        this.observers.push(observer);
    }

    notifyObservers() {
        this.observers.forEach(o => o.notify(this));
    }
}

