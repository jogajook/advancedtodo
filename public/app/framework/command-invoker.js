var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./observable"], function (require, exports, observable_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var CommandInvoker = /** @class */ (function (_super) {
        __extends(CommandInvoker, _super);
        function CommandInvoker() {
            var _this = _super.call(this) || this;
            _this.commands = [];
            _this.bindKeyboard();
            return _this;
        }
        CommandInvoker.prototype.undo = function () {
            var _this = this;
            var recentCommand = this.commands[this.commands.length - 1];
            if (!recentCommand) {
                return Promise.reject("No commands to undo");
            }
            return recentCommand.undo().then(function (result) {
                _this.commands.pop();
                _this.notifyObservers();
                return result;
            });
        };
        CommandInvoker.prototype.storeAndExecute = function (command) {
            var _this = this;
            return command.execute().then(function (result) {
                _this.commands.push(command);
                _this.notifyObservers();
                return result;
            });
        };
        CommandInvoker.prototype.getCommands = function () {
            return this.commands;
        };
        CommandInvoker.prototype.bindKeyboard = function () {
            var _this = this;
            var zKeyCode = 90;
            $(document).keydown(function (e) {
                e;
                if (e.keyCode == zKeyCode && e.ctrlKey) {
                    if (_this.commands.length > 0) {
                        _this.undo();
                    }
                }
            });
        };
        return CommandInvoker;
    }(observable_1.Observable));
    exports.CommandInvoker = CommandInvoker;
});
//# sourceMappingURL=command-invoker.js.map