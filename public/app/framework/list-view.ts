import {IView} from "./view";
import {IList} from "./list";

export abstract class ListView<T> implements IView {

    constructor(public list: IList<T>) {
    }

    abstract render();
}
