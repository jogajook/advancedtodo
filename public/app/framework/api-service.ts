export default class ApiService {

    constructor(public baseUrl, private settings = {dataType: "json", contentType: 'application/json; charset=utf-8'}) {
    }

    getList() {
        return $.get(this.baseUrl);
    }

    getOne(id) {
        return $.get(this.baseUrl + "/" + id);
    }

    create(data) {
        return $.ajax({
            type: "POST",
            url: this.baseUrl,
            data: JSON.stringify(data),
            contentType: this.settings.contentType,
            dataType: this.settings.dataType
        });
    }

    update(data) {
        return $.ajax({
            url: this.baseUrl + "/" + data.id,
            type: 'PUT',
            data: JSON.stringify(data),
            contentType: this.settings.contentType,
            dataType: this.settings.dataType
        });
    }

    delete(id) {
        return $.ajax({
            url: this.baseUrl + "/" + id,
            type: 'DELETE'
        });
    }

}
