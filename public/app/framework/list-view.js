define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ListView = /** @class */ (function () {
        function ListView(list) {
            this.list = list;
        }
        return ListView;
    }());
    exports.ListView = ListView;
});
//# sourceMappingURL=list-view.js.map