export class SpeechRecognitionWrapper {

    static createSpeechRecognition($speechBtnEl: any, opts?: ISpeechRecognitionOpts): any {
        if (window.hasOwnProperty('webkitSpeechRecognition')) {
            const recognition = new SpeechRecognitionWrapper($speechBtnEl, opts);
            return recognition;
        }
        return null;
    }

    static options = {
        lang: "ru-RU",
        runningClass: "btn-warning",
        stoppedClass: "btn-danger",
        runningBtnTitle: "Stop Speaking",
        stoppedBtnTitle: "Start Speaking",
        onResultReady: function (text) {
        }
    };

    options: ISpeechRecognitionOpts;
    recognition = new window["webkitSpeechRecognition"]();
    isRunning: boolean;

    constructor(public $speechBtnEl: any, opts?: ISpeechRecognitionOpts) {
        this.$speechBtnEl = $speechBtnEl;
        this.options = Object.assign(SpeechRecognitionWrapper.options, opts);
        this.isRunning = false;
        this.setup();
    }

    setup() {
        const opts = this.options;
        this.recognition.onresult = function (event) {
            opts.onResultReady(event.results[0][0].transcript);
        };
        this.$speechBtnEl.click(this.toggle.bind(this));
    }

    toggle() {
        this.isRunning ? this.stop() : this.start();
    }

    start() {
        this.isRunning = true;
        this.$speechBtnEl
            .removeClass(this.options.stoppedClass)
            .addClass(this.options.runningClass)
            .text(this.options.runningBtnTitle)
        ;
        this.recognition.start();
    }

    stop() {
        this.isRunning = false;
        this.$speechBtnEl
            .removeClass(this.options.runningClass)
            .addClass(this.options.stoppedClass)
            .text(this.options.stoppedBtnTitle)
        ;
        this.recognition.stop();
    }
}

export interface ISpeechRecognitionOpts {
    lang?: string,
    runningClass?: string,
    stoppedClass?: string,
    runningBtnTitle?: string,
    stoppedBtnTitle?: string,
    onResultReady?: (text: string) => any;
}
