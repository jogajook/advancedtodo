import {IObservable} from "./observable";

export interface IObserver {
    notify(observable: IObservable);
}