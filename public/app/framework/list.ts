export interface IList<Item> {
    getAll(): Item[];
    getOne(id: string): Item;
    add(item: Item): Item;
    remove(id: string): Item[];
}