define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Observable = /** @class */ (function () {
        function Observable() {
            this.observers = [];
        }
        Observable.prototype.addObserver = function (observer) {
            this.observers.push(observer);
        };
        Observable.prototype.notifyObservers = function () {
            var _this = this;
            this.observers.forEach(function (o) { return o.notify(_this); });
        };
        return Observable;
    }());
    exports.Observable = Observable;
});
//# sourceMappingURL=observable.js.map