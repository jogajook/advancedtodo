import {ICommand} from "./command";
import {Observable} from "./observable";

export interface ICommandInvoker {
    getCommands(): Array<ICommand>;
    storeAndExecute(command: ICommand): Promise<any>;
    undo(): Promise<any>;
}

export class CommandInvoker extends Observable implements ICommandInvoker {

    private commands: Array<ICommand> = [];

    constructor() {
        super();
        this.bindKeyboard();
    }

    undo(): Promise<any> {
        const recentCommand: ICommand = this.commands[this.commands.length - 1];
        if (!recentCommand) {
            return Promise.reject("No commands to undo");
        }
        return recentCommand.undo().then(result => {
            this.commands.pop();
            this.notifyObservers();
            return result;
        })
    }

    storeAndExecute(command: ICommand): Promise<any> {
        return command.execute().then(result => {
            this.commands.push(command);
            this.notifyObservers();
            return result;
        });
    }

    getCommands(): Array<ICommand> {
        return this.commands;
    }

    bindKeyboard() {
        const zKeyCode = 90;
        $(document).keydown((e) => {e;
            if (e.keyCode == zKeyCode && e.ctrlKey) {
                if (this.commands.length > 0) {
                    this.undo();
                }
            }
        })
    }
}