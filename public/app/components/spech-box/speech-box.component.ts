import {IView} from "../../framework/view";

export class SpeechBoxView implements IView {

    private $elem = $("#speech-box");
    private $todoRecorderEl = $('#todo_recorder');
    private $todoRecorderClearBtnEl = $('#todo_recorder_clear_btn');
    private $createTodosEl = $('#create-todos');
    private $selectStrategyEl = $('#select-strategy');

    constructor(private opts: ISpeechBoxViewOptions = {}) {
        // TODO: remove
        // this.setText('I wish Tim Paine came with a mute button. I\'m fine with wicket-keepers making some noise, but not constantly, and at such a high pitch. Joe Root comes down to Nathan Lyon but the fielder at mid-wicket cuts the ball off, before David Warner takes a blinding catch off Root\'s thigh pad. Lyon repeats the delivery to the same effect - the ball goes down towards Warner and through his leg, and Root jogs through for a leg bye.')
    }

    render(): SpeechBoxView {
        this.$todoRecorderEl.click(() => {
            this.onTextClick();
        });
        this.$todoRecorderClearBtnEl.click(() => {
            this.setText('');
        });
        this.$createTodosEl.click(() => {
            this.onCreateTodosClick();
        });
        this.$selectStrategyEl.change(() => {
            this.onStrategyChange();
        });
        return this;
    }

    setIsShowCreateTodos(isShow: boolean): SpeechBoxView {
        this.$createTodosEl.toggle(isShow);
        return this;
    }

    setText(text: string): SpeechBoxView {
        this.$todoRecorderEl.text(text);
        return this;
    }

    private onTextClick() {
        this.opts.onTextClickCb && this.opts.onTextClickCb.call(this, this.$todoRecorderEl.text(), this.getClickedCharPosition());
    }

    private onCreateTodosClick() {
        this.opts.onCreateTodosClickCb && this.opts.onCreateTodosClickCb.call(this, this.$todoRecorderEl.text().trim());
    }

    private onStrategyChange() {
        this.opts.onStrategyChangeCb && this.opts.onStrategyChangeCb.call(this, this.$todoRecorderEl.text(), this.$selectStrategyEl.val());
    }

    private getClickedCharPosition(): number {
        return window.getSelection().focusOffset;
    }
}

export interface ISpeechBoxViewOptions {
    onTextClickCb?: (todoSource: string, charPos: number) => any;
    onCreateTodosClickCb?: (todoSource: string) => any;
    onStrategyChangeCb?: (todoSource: string, strategy: string) => any;
}