define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SpeechBoxView = /** @class */ (function () {
        function SpeechBoxView(opts) {
            if (opts === void 0) { opts = {}; }
            this.opts = opts;
            this.$elem = $("#speech-box");
            this.$todoRecorderEl = $('#todo_recorder');
            this.$todoRecorderClearBtnEl = $('#todo_recorder_clear_btn');
            this.$createTodosEl = $('#create-todos');
            this.$selectStrategyEl = $('#select-strategy');
            // TODO: remove
            // this.setText('I wish Tim Paine came with a mute button. I\'m fine with wicket-keepers making some noise, but not constantly, and at such a high pitch. Joe Root comes down to Nathan Lyon but the fielder at mid-wicket cuts the ball off, before David Warner takes a blinding catch off Root\'s thigh pad. Lyon repeats the delivery to the same effect - the ball goes down towards Warner and through his leg, and Root jogs through for a leg bye.')
        }
        SpeechBoxView.prototype.render = function () {
            var _this = this;
            this.$todoRecorderEl.click(function () {
                _this.onTextClick();
            });
            this.$todoRecorderClearBtnEl.click(function () {
                _this.setText('');
            });
            this.$createTodosEl.click(function () {
                _this.onCreateTodosClick();
            });
            this.$selectStrategyEl.change(function () {
                _this.onStrategyChange();
            });
            return this;
        };
        SpeechBoxView.prototype.setIsShowCreateTodos = function (isShow) {
            this.$createTodosEl.toggle(isShow);
            return this;
        };
        SpeechBoxView.prototype.setText = function (text) {
            this.$todoRecorderEl.text(text);
            return this;
        };
        SpeechBoxView.prototype.onTextClick = function () {
            this.opts.onTextClickCb && this.opts.onTextClickCb.call(this, this.$todoRecorderEl.text(), this.getClickedCharPosition());
        };
        SpeechBoxView.prototype.onCreateTodosClick = function () {
            this.opts.onCreateTodosClickCb && this.opts.onCreateTodosClickCb.call(this, this.$todoRecorderEl.text().trim());
        };
        SpeechBoxView.prototype.onStrategyChange = function () {
            this.opts.onStrategyChangeCb && this.opts.onStrategyChangeCb.call(this, this.$todoRecorderEl.text(), this.$selectStrategyEl.val());
        };
        SpeechBoxView.prototype.getClickedCharPosition = function () {
            return window.getSelection().focusOffset;
        };
        return SpeechBoxView;
    }());
    exports.SpeechBoxView = SpeechBoxView;
});
//# sourceMappingURL=speech-box.component.js.map