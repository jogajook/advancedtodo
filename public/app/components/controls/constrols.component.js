define(["require", "exports", "../../framework/command-invoker"], function (require, exports, command_invoker_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var ControlView = /** @class */ (function () {
        function ControlView(commandInvoker) {
            this.commandInvoker = commandInvoker;
            this.$el = $('#controls');
            this.$undoCommandEl = this.$el.find('#undo');
            this.commandInvoker.addObserver(this);
        }
        ControlView.prototype.render = function () {
            var _this = this;
            this.$undoCommandEl.off('click');
            this.$undoCommandEl[this.isShowUndo() ? "show" : "hide"]();
            this.$undoCommandEl.click(function () {
                _this.commandInvoker.undo().then(function () {
                    _this.$undoCommandEl[_this.isShowUndo() ? "show" : "hide"]();
                });
            });
            return this;
        };
        ControlView.prototype.isShowUndo = function () {
            return this.commandInvoker.getCommands().length > 0;
        };
        ControlView.prototype.notify = function (observable) {
            if (observable instanceof command_invoker_1.CommandInvoker) {
                this.render();
            }
        };
        return ControlView;
    }());
    exports.ControlView = ControlView;
});
//# sourceMappingURL=constrols.component.js.map