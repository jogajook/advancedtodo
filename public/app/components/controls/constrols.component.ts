import {IView} from "../../framework/view";
import {IObserver} from "../../framework/observer";
import {CommandInvoker} from "../../framework/command-invoker";
import {Observable} from "../../framework/observable";

export class ControlView implements IView, IObserver {

    private $el = $('#controls');
    private $undoCommandEl = this.$el.find('#undo');

    constructor(public commandInvoker: CommandInvoker) {
        this.commandInvoker.addObserver(this);
    }

    render(): ControlView {
        this.$undoCommandEl.off('click');
        this.$undoCommandEl[this.isShowUndo() ? "show" : "hide"]();
        this.$undoCommandEl.click(() => {
            this.commandInvoker.undo().then(() => {
                this.$undoCommandEl[this.isShowUndo() ? "show" : "hide"]();
            });
        });
        return this;
    }

    isShowUndo(): boolean {
        return this.commandInvoker.getCommands().length > 0;
    }

    notify(observable: Observable) {
        if (observable instanceof CommandInvoker) {
            this.render();
        }
    }
}