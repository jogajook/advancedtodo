var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../../framework/list-view", "../../models/todo-list.model"], function (require, exports, list_view_1, todo_list_model_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TodoListView = /** @class */ (function (_super) {
        __extends(TodoListView, _super);
        function TodoListView(list, opts) {
            if (opts === void 0) { opts = {}; }
            var _this = _super.call(this, list) || this;
            _this.list = list;
            _this.opts = opts;
            _this.$elem = $("#list_container");
            _this.focusedClass = "focused";
            _this.sorTodoFunc = function (a, b) {
                if (a.status === b.status) {
                    return b.date - a.date;
                }
                else {
                    return a.isUndone() ? -1 : 1;
                }
            };
            list.addObserver(_this);
            return _this;
        }
        TodoListView.prototype.notify = function (observable) {
            if (observable instanceof todo_list_model_1.TodoList) {
                this.render();
            }
        };
        TodoListView.prototype.onCompleteClick = function (item, $todoEl) {
            this.opts.onCompleteClickCb && this.opts.onCompleteClickCb.call(this.opts.callBackContext || this, item, $todoEl);
        };
        TodoListView.prototype.onRemoveClick = function (item, $todoEl) {
            this.opts.onRemoveClickCb && this.opts.onRemoveClickCb.call(this.opts.callBackContext || this, item, $todoEl);
        };
        TodoListView.prototype.onRestoreClick = function (item, $todoEl) {
            this.opts.onRestoreClickCb && this.opts.onRestoreClickCb.call(this.opts.callBackContext || this, item, $todoEl);
        };
        TodoListView.prototype.onSaveClick = function (item, $todoEl, itemText) {
            this.opts.onSaveClickCb && this.opts.onSaveClickCb.call(this.opts.callBackContext || this, item, $todoEl, itemText);
        };
        TodoListView.prototype.render = function () {
            var _this = this;
            this.$elem.empty();
            this.list.getAll(this.sorTodoFunc).forEach(function (item) { return _this.$elem.append(_this.renderListItem(item)); });
        };
        TodoListView.prototype.getDisplayName = function (todoName) {
            return todoName.length > 25 ? (todoName.slice(0, 25) + '...') : todoName;
        };
        TodoListView.prototype.renderListItem = function (item) {
            if (item.isUndone()) {
                return this.renderListItemUndone(item);
            }
            else {
                return this.renderListItemDone(item);
            }
        };
        TodoListView.prototype.renderListItemUndone = function (item) {
            var _this = this;
            var $li = $("\n            <li data-item='" + item.id + "'>\n                <a  href='#' class=\"todo-complete\">\n                    <div data-id=\"" + item.id + "\" class='fa fa-check'></div>\n                </a>\n                <span class=\"todo-title\" title=\"" + item.name + "\">" + this.getDisplayName(item.name) + "</span>\n                <input type=\"text\" value=\"" + item.name + "\" class=\"todo-input\">\n                <span class=\"todo-date\">" + this.getNiceDate(item.date) + "</span>\n                <a href='#' class=\"todo-apply\">\n                    <div class='fa fa-paw'></div>\n                </a>\n            </li>\n        ");
            return $li
                .find(".todo-complete").click(function (e) {
                e.preventDefault();
                _this.onCompleteClick(item, $li);
            }).end()
                .find(".todo-apply").click(function (e) {
                e.preventDefault();
                _this.onSaveClick(item, $li, $li.find(".todo-input").val());
            }).end()
                .find(".todo-input").change(function (e) {
                $li.find(".todo-title").text($(e.target).val());
            }).blur(function () {
                setTimeout(function () {
                    $li.removeClass(_this.focusedClass);
                }, 300);
            }).end()
                .find(".todo-title").click(function () {
                $li.addClass(_this.focusedClass)
                    .find("input").focus();
            }).end();
        };
        TodoListView.prototype.renderListItemDone = function (item) {
            var _this = this;
            var $li = $("\n            <li data-item='" + item.id + "' class=\"todo-done\">\n                <a  href='#' class=\"todo-complete\">\n                    <div data-id=\"" + item.id + "\" class='fa fa-ambulance'></div>\n                </a>\n                <span class=\"line-through\">\n                      <span class=\"todo-title\" title=\"" + item.name + "\">" + this.getDisplayName(item.name) + "</span>\n                     <span class=\"todo-date\">" + this.getNiceDate(item.date) + "</span>\n                </span>\n                <a href='#' class=\"todo-remove\">\n                    <div class='fa fa-bomb'></div>\n                </a>\n            </li>\n        ");
            return $li
                .find(".todo-complete").click(function (e) {
                e.preventDefault();
                _this.onRestoreClick(item, $li);
            }).end()
                .find(".todo-remove").click(function (e) {
                e.preventDefault();
                _this.onRemoveClick(item, $li);
            }).end()
                .find(".todo-title").click(function () {
                $li.addClass(_this.focusedClass)
                    .find("input").focus();
            }).end();
        };
        TodoListView.prototype.getNiceDate = function (timestamp) {
            var d = new Date(timestamp);
            return "\n            " + d.getDate() + "/" + (d.getMonth() + 1) + "/" + d.getFullYear() + " " + d.getHours() + ":" + ((d.getMinutes() < 10 ? '0' : '') + d.getMinutes()) + "\n        ";
        };
        return TodoListView;
    }(list_view_1.ListView));
    exports.TodoListView = TodoListView;
});
//# sourceMappingURL=toto-list.component.js.map