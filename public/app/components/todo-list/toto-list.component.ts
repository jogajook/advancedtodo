import {ListView} from "../../framework/list-view";
import {ITodo, Todo} from "../../models/todo.model";
import {IObserver} from "../../framework/observer";
import {TodoList} from "../../models/todo-list.model";
import {Observable} from "../../framework/observable";

export class TodoListView extends ListView<Todo> implements IObserver {

    $elem = $("#list_container");
    focusedClass = "focused";

    private sorTodoFunc = (a, b) => {
        if (a.status === b.status) {
            return b.date - a.date;
        } else {
            return a.isUndone() ? -1 : 1;
        }
    };

    constructor(public list: TodoList, private opts: ITodoListViewOptions = {}) {
        super(list);
        list.addObserver(this);
    }

    notify(observable: Observable) {
        if (observable instanceof TodoList) {
            this.render();
        }
    }

    onCompleteClick(item, $todoEl) {
        this.opts.onCompleteClickCb && this.opts.onCompleteClickCb.call(this.opts.callBackContext || this, item, $todoEl);
    }

    onRemoveClick(item, $todoEl) {
        this.opts.onRemoveClickCb && this.opts.onRemoveClickCb.call(this.opts.callBackContext || this, item, $todoEl);
    }

    onRestoreClick(item, $todoEl) {
        this.opts.onRestoreClickCb && this.opts.onRestoreClickCb.call(this.opts.callBackContext || this, item, $todoEl);
    }

    onSaveClick(item, $todoEl, itemText: string) {
        this.opts.onSaveClickCb && this.opts.onSaveClickCb.call(this.opts.callBackContext || this, item, $todoEl, itemText);
    }

    render() {
        this.$elem.empty();
        this.list.getAll(this.sorTodoFunc).forEach(item => this.$elem.append(this.renderListItem(item)));
    }

    private getDisplayName(todoName: string): string {
        return todoName.length > 25 ? (todoName.slice(0, 25) + '...') : todoName;
    }

    private renderListItem(item: ITodo) {
        if (item.isUndone()) {
            return this.renderListItemUndone(item);
        } else {
            return this.renderListItemDone(item)
        }
    }

    private renderListItemUndone(item: ITodo) {
        const $li = $(`
            <li data-item='${item.id}'>
                <a  href='#' class="todo-complete">
                    <div data-id="${item.id}" class='fa fa-check'></div>
                </a>
                <span class="todo-title" title="${item.name}">${this.getDisplayName(item.name)}</span>
                <input type="text" value="${item.name}" class="todo-input">
                <span class="todo-date">${this.getNiceDate(item.date)}</span>
                <a href='#' class="todo-apply">
                    <div class='fa fa-paw'></div>
                </a>
            </li>
        `);

        return $li
            .find(".todo-complete").click((e) => {
                e.preventDefault();
                this.onCompleteClick(item, $li);
            }).end()
            .find(".todo-apply").click((e) => {
                e.preventDefault();
                this.onSaveClick(item, $li, $li.find(".todo-input").val());
            }).end()
            .find(".todo-input").change((e) => {
                $li.find(".todo-title").text($(e.target).val());
            }).blur(() => {
                setTimeout(() => {
                    $li.removeClass(this.focusedClass)
                }, 300);
            }).end()
            .find(".todo-title").click(() => {
                $li.addClass(this.focusedClass)
                    .find("input").focus();
            }).end();
    }

    private renderListItemDone(item: ITodo) {
        const $li = $(`
            <li data-item='${item.id}' class="todo-done">
                <a  href='#' class="todo-complete">
                    <div data-id="${item.id}" class='fa fa-ambulance'></div>
                </a>
                <span class="line-through">
                      <span class="todo-title" title="${item.name}">${this.getDisplayName(item.name)}</span>
                     <span class="todo-date">${this.getNiceDate(item.date)}</span>
                </span>
                <a href='#' class="todo-remove">
                    <div class='fa fa-bomb'></div>
                </a>
            </li>
        `);

        return $li
            .find(".todo-complete").click((e) => {
                e.preventDefault();
                this.onRestoreClick(item, $li);
            }).end()
            .find(".todo-remove").click((e) => {
                e.preventDefault();
                this.onRemoveClick(item, $li);
            }).end()
            .find(".todo-title").click(() => {
                $li.addClass(this.focusedClass)
                    .find("input").focus();
            }).end();
    }

    private getNiceDate(timestamp: string | number): string {
        const d = new Date(timestamp);
        return `
            ${d.getDate()}/${(d.getMonth() + 1)}/${d.getFullYear()} ${d.getHours()}:${(d.getMinutes() < 10 ? '0' : '') + d.getMinutes()}
        `;
    }

}

export interface ITodoListViewOptions {
    onCompleteClickCb?: (item: any, $todoEl: any) => any;
    onRemoveClickCb?: (item: any, $todoEl: any) => any;
    onRestoreClickCb?: (item: any, $todoEl: any) => any;
    onSaveClickCb?: (item: any, $todoEl: any, itemText: string) => any;
    callBackContext?: any;
}
