define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AppConfig = /** @class */ (function () {
        function AppConfig() {
        }
        AppConfig.apiBaseUrl = "http://localhost:3000/api/todo";
        return AppConfig;
    }());
    exports.AppConfig = AppConfig;
});
//# sourceMappingURL=config.model.js.map