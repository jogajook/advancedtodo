var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "../framework/observable", "./todo.model"], function (require, exports, observable_1, todo_model_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TodoList = /** @class */ (function (_super) {
        __extends(TodoList, _super);
        function TodoList() {
            var _this = _super !== null && _super.apply(this, arguments) || this;
            _this.list = [];
            return _this;
        }
        TodoList.prototype.getAll = function (sortFunc) {
            return sortFunc ? this.list.sort(sortFunc) : this.list;
        };
        TodoList.prototype.getOne = function (id) {
            return this.list.filter(function (item) { return item.id === id; })[0];
        };
        TodoList.prototype.add = function (todo) {
            todo.addObserver(this);
            this.list.push(todo);
            this.notifyObservers();
            return todo;
        };
        TodoList.prototype.remove = function (id) {
            this.list.splice(this.list.indexOf(this.getOne(id)), 1);
            this.notifyObservers();
            return this.list;
        };
        TodoList.prototype.notify = function (observable) {
            if (observable instanceof todo_model_1.Todo) {
                this.notifyObservers();
            }
        };
        return TodoList;
    }(observable_1.Observable));
    exports.TodoList = TodoList;
});
//# sourceMappingURL=todo-list.model.js.map