import {Status} from "./status.model";
import {Observable} from "../framework/observable";

export interface ITodo {
    id: string;
    name: string;
    status: Status;
    date: number;
    setDone(): ITodo;
    setUndone(): ITodo;
    isDone(): boolean;
    isUndone(): boolean;
    setName(name: string): ITodo;
}


export class Todo extends Observable implements ITodo {

    constructor(public name: string,
                public id: string = null,
                public date = new Date().getTime(),
                public status: Status = Status.Undone) {
        super();
    }

    setName(name: string): ITodo {
        this.update({name: name});
        return this;
    }

    setDone(): ITodo {
        this.setStatus(Status.Done);
        return this;
    }


    setUndone(): ITodo {
        this.setStatus(Status.Undone);
        return this;
    }

    isDone() {
        return this.status === Status.Done;
    }

    isUndone() {
        return this.status === Status.Undone;
    }

    toJSON() {
        const obj = Object.assign({}, this);
        delete obj.observers;
        return obj;
    }

    private setStatus(status: Status): ITodo {
        this.update({status: status});
        return this;
    }

    private update(params: { name?: string, status?: Status }): ITodo {
        for (let key in params) {
            if (this.hasOwnProperty(key)) {
                this[key] = params[key];
            }
        }
        this.notifyObservers();
        return this;
    }

}