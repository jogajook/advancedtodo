define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TokenizerDecorator = /** @class */ (function () {
        function TokenizerDecorator(tokenizer) {
            this.tokenizer = tokenizer;
        }
        TokenizerDecorator.prototype.setTokenizeStrategy = function (strategy) {
            return this.tokenizer.setTokenizeStrategy(strategy);
        };
        return TokenizerDecorator;
    }());
    exports.TokenizerDecorator = TokenizerDecorator;
});
//# sourceMappingURL=tokenizer-decorator.model.js.map