define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var SpaceTokenizeStrategy = /** @class */ (function () {
        function SpaceTokenizeStrategy() {
        }
        SpaceTokenizeStrategy.clean = function (source) {
            return source.replace(/\s\s+/g, " ");
        };
        SpaceTokenizeStrategy.prototype.process = function (source) {
            source = SpaceTokenizeStrategy.clean(source);
            return source ? source.split(" ") : [];
        };
        return SpaceTokenizeStrategy;
    }());
    exports.SpaceTokenizeStrategy = SpaceTokenizeStrategy;
});
//# sourceMappingURL=tokenize-strategy-space.model.js.map