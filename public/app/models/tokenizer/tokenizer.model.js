define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Tokenizer = /** @class */ (function () {
        function Tokenizer(tokenizeStrategy) {
            this.tokenizeStrategy = tokenizeStrategy;
        }
        Tokenizer.prototype.setTokenizeStrategy = function (strategy) {
            this.tokenizeStrategy = strategy;
            return this;
        };
        Tokenizer.prototype.tokenize = function (source, params) {
            return this.tokenizeStrategy.process(source, params);
        };
        return Tokenizer;
    }());
    exports.Tokenizer = Tokenizer;
});
//# sourceMappingURL=tokenizer.model.js.map