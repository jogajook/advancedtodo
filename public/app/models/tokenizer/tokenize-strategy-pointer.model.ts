import {TokenizeStrategyParams} from "./tokenizer.model";
import {TokenizeStrategy} from "./tokenize-strategy.model";

export class PointerTokenizeStrategy implements TokenizeStrategy {

    static substringToChar(source: string, charPos: number, includeChar: boolean = false): string {
        let endIndex: number = charPos + (includeChar && 1 || 0);
        return source.substring(0, endIndex);
    }

    process(source: string, params: TokenizeStrategyParams): string[] {
        const word = PointerTokenizeStrategy.substringToChar(source, params.endCharPos);
        return new Array<string>(word);
    }
}
