import {TokenizeStrategyParams} from "./tokenizer.model";

export interface TokenizeStrategy {
    process(source: string, params?: TokenizeStrategyParams): string[];
}