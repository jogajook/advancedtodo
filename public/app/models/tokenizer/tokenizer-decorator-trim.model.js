var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./tokenizer-decorator.model"], function (require, exports, tokenizer_decorator_model_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TrimTokenizerDecorator = /** @class */ (function (_super) {
        __extends(TrimTokenizerDecorator, _super);
        function TrimTokenizerDecorator(tokenizer) {
            var _this = _super.call(this, tokenizer) || this;
            _this.tokenizer = tokenizer;
            return _this;
        }
        TrimTokenizerDecorator.prototype.tokenize = function (source, params) {
            return this.tokenizer.tokenize(source, params).map(function (w) { return w.trim(); });
        };
        return TrimTokenizerDecorator;
    }(tokenizer_decorator_model_1.TokenizerDecorator));
    exports.TrimTokenizerDecorator = TrimTokenizerDecorator;
});
//# sourceMappingURL=tokenizer-decorator-trim.model.js.map