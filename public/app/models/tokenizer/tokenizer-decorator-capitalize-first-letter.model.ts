import {ITokenizer, TokenizeStrategyParams} from "./tokenizer.model";
import {TokenizerDecorator} from "./tokenizer-decorator.model";

export class CapitalizeFirstLetterTokenizerDecorator extends TokenizerDecorator {

    constructor(public tokenizer: ITokenizer) {
        super(tokenizer);
    }

    tokenize(source: string, params?: TokenizeStrategyParams): string[] {
        return this.tokenizer.tokenize(source, params).map(w => CapitalizeFirstLetterTokenizerDecorator.capitalizeFirstLetter(w));
    }

    static capitalizeFirstLetter(source: string): string {
        return source.charAt(0).toUpperCase() + source.slice(1);
    }
}