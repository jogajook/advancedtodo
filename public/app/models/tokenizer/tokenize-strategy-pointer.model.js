define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var PointerTokenizeStrategy = /** @class */ (function () {
        function PointerTokenizeStrategy() {
        }
        PointerTokenizeStrategy.substringToChar = function (source, charPos, includeChar) {
            if (includeChar === void 0) { includeChar = false; }
            var endIndex = charPos + (includeChar && 1 || 0);
            return source.substring(0, endIndex);
        };
        PointerTokenizeStrategy.prototype.process = function (source, params) {
            var word = PointerTokenizeStrategy.substringToChar(source, params.endCharPos);
            return new Array(word);
        };
        return PointerTokenizeStrategy;
    }());
    exports.PointerTokenizeStrategy = PointerTokenizeStrategy;
});
//# sourceMappingURL=tokenize-strategy-pointer.model.js.map