import {TokenizeStrategy} from "./tokenize-strategy.model";

export class SpaceTokenizeStrategy implements TokenizeStrategy {

    static clean(source: string): string {
        return source.replace(/\s\s+/g, " ");
    }

    process(source: string): string[] {
        source = SpaceTokenizeStrategy.clean(source);
        return source ? source.split(" ") : [];
    }
}