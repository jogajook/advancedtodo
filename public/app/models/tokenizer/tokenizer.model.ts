import {TokenizeStrategy} from "./tokenize-strategy.model";

export interface ITokenizer {
    setTokenizeStrategy(strategy: TokenizeStrategy): Tokenizer;
    tokenize(source: string, params?: TokenizeStrategyParams): string[];
}

export interface TokenizeStrategyParams {
    endCharPos: number;
}

export class Tokenizer implements ITokenizer {

    constructor(public tokenizeStrategy: TokenizeStrategy) {
    }

    setTokenizeStrategy(strategy: TokenizeStrategy): Tokenizer {
        this.tokenizeStrategy = strategy;
        return this;
    }

    tokenize(source: string, params?: TokenizeStrategyParams): string[] {
        return this.tokenizeStrategy.process(source, params);
    }
}
