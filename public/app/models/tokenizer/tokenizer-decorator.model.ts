import {ITokenizer, Tokenizer, TokenizeStrategyParams} from "./tokenizer.model";
import {TokenizeStrategy} from "./tokenize-strategy.model";

export abstract class TokenizerDecorator implements ITokenizer {

    constructor(public tokenizer: ITokenizer) {
    }

    setTokenizeStrategy(strategy: TokenizeStrategy): Tokenizer {
        return this.tokenizer.setTokenizeStrategy(strategy);
    }

    abstract tokenize(source: string, params?: TokenizeStrategyParams): string[];
}