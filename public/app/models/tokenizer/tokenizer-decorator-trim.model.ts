import {ITokenizer, TokenizeStrategyParams} from "./tokenizer.model";
import {TokenizerDecorator} from "./tokenizer-decorator.model";

export class TrimTokenizerDecorator extends TokenizerDecorator {

    constructor(public tokenizer: ITokenizer) {
        super(tokenizer);
    }

    tokenize(source: string, params?: TokenizeStrategyParams): string[] {
        return this.tokenizer.tokenize(source, params).map(w => w.trim());
    }
}