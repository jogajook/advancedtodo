define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var TodoStrategyOptions;
    (function (TodoStrategyOptions) {
        TodoStrategyOptions[TodoStrategyOptions["Pointer"] = 1] = "Pointer";
        TodoStrategyOptions[TodoStrategyOptions["SpaceSeparatorSpaceSeparator"] = 2] = "SpaceSeparatorSpaceSeparator";
    })(TodoStrategyOptions = exports.TodoStrategyOptions || (exports.TodoStrategyOptions = {}));
});
//# sourceMappingURL=todo-strategy-options.model.js.map