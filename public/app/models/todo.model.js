var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
define(["require", "exports", "./status.model", "../framework/observable"], function (require, exports, status_model_1, observable_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Todo = /** @class */ (function (_super) {
        __extends(Todo, _super);
        function Todo(name, id, date, status) {
            if (id === void 0) { id = null; }
            if (date === void 0) { date = new Date().getTime(); }
            if (status === void 0) { status = status_model_1.Status.Undone; }
            var _this = _super.call(this) || this;
            _this.name = name;
            _this.id = id;
            _this.date = date;
            _this.status = status;
            return _this;
        }
        Todo.prototype.setName = function (name) {
            this.update({ name: name });
            return this;
        };
        Todo.prototype.setDone = function () {
            this.setStatus(status_model_1.Status.Done);
            return this;
        };
        Todo.prototype.setUndone = function () {
            this.setStatus(status_model_1.Status.Undone);
            return this;
        };
        Todo.prototype.isDone = function () {
            return this.status === status_model_1.Status.Done;
        };
        Todo.prototype.isUndone = function () {
            return this.status === status_model_1.Status.Undone;
        };
        Todo.prototype.toJSON = function () {
            var obj = Object.assign({}, this);
            delete obj.observers;
            return obj;
        };
        Todo.prototype.setStatus = function (status) {
            this.update({ status: status });
            return this;
        };
        Todo.prototype.update = function (params) {
            for (var key in params) {
                if (this.hasOwnProperty(key)) {
                    this[key] = params[key];
                }
            }
            this.notifyObservers();
            return this;
        };
        return Todo;
    }(observable_1.Observable));
    exports.Todo = Todo;
});
//# sourceMappingURL=todo.model.js.map