import ApiService from "../framework/api-service";
import {Todo} from "./todo.model";
import {TodoList} from "./todo-list.model";
import {ICommand} from "../framework/command";

export class AddTodoCommand implements ICommand {

    constructor(private todoList: TodoList, private todo: Todo, private apiService: ApiService) {
    }

    execute(): Promise<any> {
        return new Promise(resolve => {
            this.apiService.create(this.todo).then((todo: Todo) => {
                Object.assign(this.todo, todo);
                resolve(this.todoList.add(this.todo))
            });
        });
    }

    undo(): Promise<any> {
        return new Promise(resolve => {
            this.apiService.delete(this.todo.id).then(() =>
                resolve(this.todoList.remove(this.todo.id))
            );
        });
    }
}

export class RemoveTodoCommand implements ICommand {

    constructor(private todoList: TodoList, private todo: Todo, private apiService: ApiService) {
    }

    execute(): Promise<any> {
        return new Promise(resolve => {
            this.apiService.delete(this.todo.id).then(() =>
                resolve(this.todoList.remove(this.todo.id))
            );
        });
    }

    undo(): Promise<any> {
        return new Promise(resolve => {
            this.apiService.create(this.todo).then((todo: Todo) => {
                Object.assign(this.todo, todo);
                resolve(this.todoList.add(this.todo))
            });
        });
    }
}
