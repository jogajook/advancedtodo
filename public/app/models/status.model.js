define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Status;
    (function (Status) {
        Status[Status["Undone"] = 0] = "Undone";
        Status[Status["Done"] = 1] = "Done";
    })(Status = exports.Status || (exports.Status = {}));
});
//# sourceMappingURL=status.model.js.map