define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var AddTodoCommand = /** @class */ (function () {
        function AddTodoCommand(todoList, todo, apiService) {
            this.todoList = todoList;
            this.todo = todo;
            this.apiService = apiService;
        }
        AddTodoCommand.prototype.execute = function () {
            var _this = this;
            return new Promise(function (resolve) {
                _this.apiService.create(_this.todo).then(function (todo) {
                    Object.assign(_this.todo, todo);
                    resolve(_this.todoList.add(_this.todo));
                });
            });
        };
        AddTodoCommand.prototype.undo = function () {
            var _this = this;
            return new Promise(function (resolve) {
                _this.apiService.delete(_this.todo.id).then(function () {
                    return resolve(_this.todoList.remove(_this.todo.id));
                });
            });
        };
        return AddTodoCommand;
    }());
    exports.AddTodoCommand = AddTodoCommand;
    var RemoveTodoCommand = /** @class */ (function () {
        function RemoveTodoCommand(todoList, todo, apiService) {
            this.todoList = todoList;
            this.todo = todo;
            this.apiService = apiService;
        }
        RemoveTodoCommand.prototype.execute = function () {
            var _this = this;
            return new Promise(function (resolve) {
                _this.apiService.delete(_this.todo.id).then(function () {
                    return resolve(_this.todoList.remove(_this.todo.id));
                });
            });
        };
        RemoveTodoCommand.prototype.undo = function () {
            var _this = this;
            return new Promise(function (resolve) {
                _this.apiService.create(_this.todo).then(function (todo) {
                    Object.assign(_this.todo, todo);
                    resolve(_this.todoList.add(_this.todo));
                });
            });
        };
        return RemoveTodoCommand;
    }());
    exports.RemoveTodoCommand = RemoveTodoCommand;
});
//# sourceMappingURL=commands.model.js.map