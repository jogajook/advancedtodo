import {Observable} from "../framework/observable";
import {IList} from "../framework/list";
import {Todo} from "./todo.model";
import {IObserver} from "../framework/observer";

export class TodoList extends Observable implements IList<Todo>, IObserver {

    private list: Todo[] = [];

    getAll(sortFunc?: (a: Todo, b: Todo) => number): Todo[] {
        return sortFunc ? this.list.sort(sortFunc) : this.list;
    }

    getOne(id: string): Todo {
        return this.list.filter(item => item.id === id)[0];
    }

    add(todo: Todo): Todo {
        todo.addObserver(this);
        this.list.push(todo);
        this.notifyObservers();
        return todo;
    }

    remove(id: string): Todo[] {
        this.list.splice(this.list.indexOf(this.getOne(id)), 1);
        this.notifyObservers();
        return this.list;
    }

    notify(observable: Observable) {
        if (observable instanceof Todo) {
            this.notifyObservers();
        }
    }
}