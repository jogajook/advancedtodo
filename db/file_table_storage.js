var fs = require('fs');
var path = require('path');


class FileTableStorage {

    constructor(filePath) {
        this.filePath = filePath || path.resolve(__dirname, "./db.txt");
        this._setup();
        this.rows = this._readFromFile();
    }


    /*** Interface ***/

    getAll() {
        return this._return(this.rows);
    }

    getById(id) {
        return this._return(this.rows.find(row => row.id === id));
    }

    create(newRow) {
        let dt = newRow.date = new Date().getTime();
        newRow.id = dt.toString();
        this.rows.push(newRow);
        this._sync();
        return this._return(newRow);
    }

    update(updatedRow) {
        let row = this.rows.find(row => row.id === updatedRow.id);
        if (row) {
            Object.assign(row, updatedRow);
        }
        return this._return(row);
    }

    delete(id) {
        let row = this.rows.find(row => row.id === id);
        if (row) {
            this.rows.splice(this.rows.indexOf(row), 1);
            this._sync();
            return true;
        }
        return false;
    }


    /*** Private ***/

    _setup() {
        if (!this._readFromFile()) {
            this._writeToFile([]);
        }
    }

    _sync() {
        this._writeToFile(this.rows);
    }

    _return(data) {
        return this._copy(data);
    }

    _copy(obj) {
        return JSON.parse(JSON.stringify(obj))
    }

    _writeToFile(data) {
        fs.writeFileSync(this.filePath, JSON.stringify(data));
    }

    _readFromFile() {
        let data = fs.readFileSync(this.filePath).toString();
        if (!data) {
            return data;
        }
        return JSON.parse(data);
    }
}


module.exports = FileTableStorage;