var express = require('express');
var router = express.Router();
var Storage = require('../db/file_table_storage');
var fs = require('fs');
var path = require('path');

var storage = new Storage();

router.route('/')
    .get(function (req, res, next) {
        var fileName = 'front_page.html';
        let html = fs.readFileSync(path.join(req.app.get('views'), fileName));
        res.render('index', { html: html });
    });

// Api
router.route('/api/todo')
    .get(function (req, res, next) {
        res.json(storage.getAll());
    })
    .post(function (req, res, next) {
        res.json(storage.create(req.body));
    });

router.route('/api/todo/:id')
    .all(function (req, res, next) {
        next();
    })
    .get(function (req, res, next) {
        res.json(storage.getAll());
    })
    .put(function (req, res, next) {
        console.log("updating todo", req.body);
        res.json(storage.update(req.body));
    })
    .delete(function (req, res, next) {
        console.log("deleting todo", req.params.id);
        res.json({success: storage.delete(req.params.id)});
    });

module.exports = router;
